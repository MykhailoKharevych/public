import { UserService } from './../services/user.service';
import { SignalRService } from './../services/signal-r.service';
import { MessageFactoryService } from './../services/message-factory.service';
import { MessageComponent } from './../message/message.component';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder  } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { environment } from '../../environments/environment';
import { ChatModel } from '../interfaces/chat-model';
import { NgElement, WithProperties } from '@angular/elements';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})

export class ChatComponent implements OnInit {
  entranceForm: FormGroup;
  currentLogin: '';
  loginTyping: string;
  baseUrl: string;
  chatNotifications: ChatModel[];
  filteredSenders: string[];
  messageBuilder: any;
  isShowTyping: boolean;
  searchTerm: string;
  senders: string[];
  private hubConnection: any;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private messageFactory: MessageFactoryService,
    private signalRService: SignalRService,
    private userService: UserService) {
    this.createForms();
    this.baseUrl = environment.apiUrl;
    // this.hubConnection = signalRService.hubConnection;
  }

  createForms() {
    this.entranceForm = this.fb.group({
      nick: [''],
      messageInput: [''],
      searchTerm: ['']
    });
  }

  ngOnInit() {
    this.getNotifications().subscribe();
    this.onFormChanges();
    // this.appendMessageLine();
  }

  continueToChat() {
    this.currentLogin = this.entranceForm.get('nick').value;
    if ( this.currentLogin.length ) {
      this.signalRService.startConnectionHub();
      this.hubConnection = this.signalRService.hubConnection;
      this.subscribeOnHub();

      const addNickUrl = `/api/notification/addnick?newLogin=${this.currentLogin}`;
      this.http.get(addNickUrl)
       .subscribe(res => {
         console.log(res);
         $('#entrance').hide();
       });
    }
  }

  sendMessage() {
    const body: ChatModel = {
      login: this.currentLogin,
      messageText: this.entranceForm.get('messageInput').value,
    };

    if (!body.login || !body.messageText) {
      return;
    }

    this.http.post('/api/notification/send/', body)
      .subscribe(res => {
        console.log(res);
      });
    this.entranceForm.patchValue({message: ''});
  }

  onKeydown(event) {
    if (event.keyCode === 13) {
      if (!event.shiftKey) {
        event.preventDefault();
        this.sendMessage();
      }
    } else {
      this.typingMessage();
    }
  }

  userIsTyping() {
    if (this.isMethodSubscribe('typing')) {
      return false;
    }

    this.hubConnection.on('Typing', (nick) => {
      this.loginTyping = nick;
      this.isShowTyping = nick !== this.currentLogin;
    });

    if (this.isMethodSubscribe('stoptyping')) {
      return false;
    }

    this.hubConnection.on('StopTyping', (nick) => {
      // user stop typing. Need hide 'is typing' message
      this.isShowTyping = this.loginTyping !== nick;
    });
  }

  private getNotifications(): Observable<any> {
    const readNotificationsUrl = '/api/notification/read/';

    return this.http.get<any>(readNotificationsUrl)
     .pipe(
       map(x => x as ChatModel[]),
       tap(x => this.chatNotifications = x)
     );
  }

  private onFormChanges(): void {
    this.entranceForm.get('searchTerm').valueChanges.subscribe(val => {
      this.searchTerm = val;
    });
  }

  private subscribeOnHub() {
    this.userIsTyping();
    this.updateMessage();
    this.updateSenders();
  }

  private typingMessage() {
    if (!this.currentLogin) {
      return;
    }
    const typingUrl = `/api/notification/useristyping?login=${this.currentLogin}`;
    this.http.get(typingUrl)
     .subscribe(res => {
       console.log(res);
     });
  }

  private updateMessage() {
    if (this.isMethodSubscribe('send')) {
      return false;
    }

    this.hubConnection.on('Send', (data) => {
      this.chatNotifications.push(data);
    });
  }

  private updateSenders() {
    if (this.isMethodSubscribe('senders')) {
      return false;
    }

    this.hubConnection.on('Senders', (data) => {
      this.senders = data;
    });
  }

  private isMethodSubscribe(methodName) {
    return this.hubConnection && this.hubConnection.methods[methodName];
  }

  // private getUsers() {
  //   this.userService.getAll().subscribe(users => {
  //     this.senders = users;
  // });
  // }
}
