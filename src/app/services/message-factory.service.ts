import { Injectable, Injector, OnInit } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { MessageComponent } from '../message/message.component';

@Injectable({
  providedIn: 'root'
})
export class MessageFactoryService {

  constructor(injector: Injector) {
    this.createCustomElement(injector);
  }

  private createCustomElement(injector: Injector) {
    const messageBuilder = createCustomElement(MessageComponent, {injector});
    // Register the custom element with the browser.
    customElements.define('app-message', messageBuilder);
  }
}
