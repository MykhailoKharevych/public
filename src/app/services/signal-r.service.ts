import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import * as signalR from '@aspnet/signalr';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  hubConnection: signalR.HubConnection;

  constructor() {
    // this.startConnectionHub();
  }

  startConnectionHub() {
    // requestinterceptor doesn't work here  . Need HTTP client
    const connectUrl = `${environment.apiUrl}/chat`;

    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(connectUrl)
      .build();

    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err));
  }
}
