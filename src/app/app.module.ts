import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ChatComponent } from './chat/chat.component';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
        //  MatButtonModule,
        //  MatCheckboxModule,
        //  MatNativeDateModule,
        //  MatSnackBarModule,
          MatIconModule,
        //  MatDialogModule,
        //  MatTableModule,
        //  MatPaginatorModule ,
        //  MatSortModule,
        //  MatTabsModule,
        //  MatToolbarModule,
        //  MatCardModule,
        //  MatFormFieldModule,
        //  MatProgressSpinnerModule,
        //  MatInputModule
        } from '@angular/material';
import { RequestInterceptor } from './interceptors/request.interceptor';
import { MessageComponent } from './message/message.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { HomeComponent } from './home/home.component';
import { FilterPipe } from './pipes/filter.pipe';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ChatComponent,
    MessageComponent,
    PrivacyComponent,
    HomeComponent,
    FilterPipe,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule
  ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    }
  ],
  entryComponents: [MessageComponent]

})
export class AppModule { }
