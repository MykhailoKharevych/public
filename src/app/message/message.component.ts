import { ChatModel } from './../interfaces/chat-model';
import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnChanges {
  messageClass: string;
  messageContainerClass = 'd-flex mb-4';

  @Input() chat: ChatModel;
  @Input() isCurrentUserSendMessage: boolean;

  constructor() {
    this.chat = {
      login: '',
      messageText: ''
    };
  }

  // chat1: ChatModel;
  // @Input() isTyping: boolean;

  ngOnChanges() {
    this.initMessage();
  }

  // Messages couldn't be edited or removed
  private initMessage() {
    if (this.chat) {
        this.messageClass = this.isCurrentUserSendMessage ? 'msg_container_send' : 'msg_container';
        this.messageContainerClass = this.messageContainerClass
          + ( this.isCurrentUserSendMessage ? ' justify-content-end' : ' justify-content-start');
    }
  }
}
